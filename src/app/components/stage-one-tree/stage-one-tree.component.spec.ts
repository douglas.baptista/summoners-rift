import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StageOneTreeComponent } from './stage-one-tree.component';

describe('StageOneTreeComponent', () => {
  let component: StageOneTreeComponent;
  let fixture: ComponentFixture<StageOneTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StageOneTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StageOneTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
