import { Component, Input, OnInit } from '@angular/core';
import { BehaviorService } from '@niboServices/behavior/behavior.service';
import { Players } from '@niboRoot/interfaces/players';
import { Player } from '@niboRoot/interfaces/player';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-stage-one',
  templateUrl: './stage-one-tree.component.html',
  styleUrls: ['./stage-one-tree.component.scss'],
})
export class StageOneTreeComponent implements OnInit {
  @Input() dataA: Array<Player>;
  @Input() dataB: Array<Player>;

  constructor(private behaviorService: BehaviorService) {
  }

  ngOnInit(): void {
  }

  selectWinner(item: Player): void {

    if (item.keyOrigin === 'A' && item.status === '') {
      item.status = 'semi';
      this.behaviorService.selectWinner(item);
    }
    if (item.keyOrigin === 'B' && item.status === '') {
      item.status = 'semi';
      this.behaviorService.selectWinner(item);
    }
  }

}
