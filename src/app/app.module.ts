import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Default Imports
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';

// Pages Imports
import { HomePageComponent } from '@niboPages/home-page/home-page.component';
import { NotFoundPageComponent } from '@niboPages/not-found-page/not-found-page.component';

// Components Imports
import { StageOneTreeComponent } from '@niboComponents/stage-one-tree/stage-one-tree.component';
import { StageTwoTreeComponent } from '@niboComponents/stage-two-tree/stage-two-tree.component';
import { StageThreeTreeComponent } from '@niboComponents/stage-three-tree/stage-three-tree.component';

// Services Imports
import { BehaviorService } from '@niboServices/behavior/behavior.service';
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NotFoundPageComponent,
    StageOneTreeComponent,
    StageTwoTreeComponent,
    StageThreeTreeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    BehaviorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
