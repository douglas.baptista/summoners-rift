# SummonersRift

Esse projeto foi criado utilizando angular cli versão 10.1.3

O objetivo é gerar um chave de torneio onde é possível selecionar os jogadores e definir quem avança no sistema de chave.

Para o uso do aplicativo é necessário utilizar a biblioteca Json Server `https://github.com/typicode/json-server`. Infelizmente na versão atual da aplicação ainda não foi implementada um botão de reset dos players e por isso é necessário limpar o db.json manualmente.

A aplicação possui alguns serviços como comunicação entre componentes e também serviços para requisições genéricas HTTP.

Para maiores dúvidas : baptista.dev@gmail.com

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
