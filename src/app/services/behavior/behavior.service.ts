import { Injectable } from '@angular/core';
import { Player } from '@niboRoot/interfaces/player';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BehaviorService {
  
  behaviorService: BehaviorSubject<any>
  constructor(
  ) { 
    this.behaviorService = new BehaviorSubject({});
  }

  getPlayer(): Observable<Player> {
    return this.behaviorService.asObservable();
}

  selectWinner(item) {
      this.behaviorService.next(item);
  }
}
