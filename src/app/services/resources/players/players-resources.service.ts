import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Player } from '@niboRoot/interfaces/player';
import { RequestService } from '../../request/request.service';
import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlayersResourcesService extends RequestService<Player, string> {

  constructor(
    private HttpClient: HttpClient
  ) {
    super( HttpClient, `${environment.api.baseUrl}/players`);
  }
}
