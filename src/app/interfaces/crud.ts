import { Observable } from 'rxjs';

export interface Crud<T, ID> {
    getAll(): Observable<T[]>;
    post(data: T): Observable<T>;
    put(id: ID, data: T): Observable<T>;
    getById(id: ID): Observable<T>;
    delete(id: ID): Observable<any>;
  }