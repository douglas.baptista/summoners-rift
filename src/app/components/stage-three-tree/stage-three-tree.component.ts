import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-stage-three',
  templateUrl: './stage-three-tree.component.html',
  styleUrls: ['./stage-three-tree.component.scss'],
})
export class StageThreeTreeComponent implements OnInit {
  @Input() data: Observable<any>;
  _data: object = {
    name: '',
    status: '',
    keyOrigin: '',
  };
  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes): void {
    if (this.data['status'] === 'winner') {
      this._data = this.data;
    }
  }
}
