import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StageThreeTreeComponent } from './stage-three-tree.component';

describe('StageThreeTreeComponent', () => {
  let component: StageThreeTreeComponent;
  let fixture: ComponentFixture<StageThreeTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StageThreeTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StageThreeTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
