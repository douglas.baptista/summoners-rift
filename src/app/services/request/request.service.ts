import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Crud } from '@niboRoot/interfaces/crud';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class RequestService<T, ID> implements Crud<T, ID> {

  constructor(
    private _HTTP: HttpClient,
    private _API: string,
  ) { }

  public getAll(): Observable<T[]> {
    return this._HTTP.get<T[]>(`${this._API}`);
  }

  public post(data: T): Observable<T> {
    return this._HTTP.post<T>(`${this._API}`, data);
  }

  public put(id: ID, data: T): Observable<T> {
    return this._HTTP.put<T>(`${this._API}/${id}`, data, {});
  }

  public getById(id: ID): Observable<T> {
    return this._HTTP.get<T>(`${this._API}/${id}`);
  }

  public delete(id: ID): Observable<T> {
    return this._HTTP.delete<T>(`${this._API}/${id}`);
  }

}
