import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorService } from '@niboServices/behavior/behavior.service';
import { Player } from '@niboRoot/interfaces/player';

@Component({
  selector: 'app-stage-two',
  templateUrl: './stage-two-tree.component.html',
  styleUrls: ['./stage-two-tree.component.scss'],
})
export class StageTwoTreeComponent implements OnInit {
  @Input() playerSemiA: Player;
  @Input() playerSemiB: Player;
  constructor(private behaviorService: BehaviorService) {}

  ngOnInit(): void {
  }

  selectWinner(item:Player) {
    item.status = 'winner';
    this.behaviorService.selectWinner(item);
  }
}
