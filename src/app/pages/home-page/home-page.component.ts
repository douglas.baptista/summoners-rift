import { Component, OnInit } from '@angular/core';
import { BehaviorService } from '@niboServices/behavior/behavior.service';
import { Players } from '@niboRoot/interfaces/players';
import { PlayersResourcesService } from '@niboRoot/services/resources/players/players-resources.service';
import { KeyAResourcesService } from '@niboRoot/services/resources/keyA/keyA-resources.service';
import { KeyBResourcesService } from '@niboRoot/services/resources/keyB/keyB-resources.service';
import { Player } from '@niboRoot/interfaces/player';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {

  techs: Array<any> = [];
  selectTechs = [];
  playersKeyA = [];
  playersKeyB = [];
  playersSemiA: Player;
  playersSemiB: Player;
  champion: Object = {};
  constructor(
    private behaviorService: BehaviorService,
    private playerRequest: PlayersResourcesService
    ) {
    }

  ngOnInit(): void {
    this.getAllTechs();
    this.behaviorService.getPlayer().subscribe((player: any) => {
      if (player.status === 'semi' && player.keyOrigin === 'A') {
        this.playersSemiA = player;
      } else if (player.status === 'semi' && player.keyOrigin === 'B') {
        this.playersSemiB = player;
      }
      if (player.status === 'winner') {
        this.champion = player;
      }
      this.playerRequest.put(player.id, player).subscribe();
    });
  }

  getAllTechs(): void {
    this.playerRequest.getAll().subscribe(item => this.techs = item);
    this.playerRequest.getAll().subscribe(item => {
      this.playersKeyA = item.filter(item => {
        return item.keyOrigin === 'A';
      });
    });
    this.playerRequest.getAll().subscribe(item => {
      this.playersKeyB = item.filter(item => {
        return item.keyOrigin === 'B';
      });
    });
  }

  selectTech(data: Player): void {
    this.selectTechs.push(data);
    if (this.selectTechs.length < 3) {
      data.selected = true;
      data.keyOrigin = 'A';
      this.playerRequest.put(data.id, data).subscribe();

    } else if (!data.selected) {
      data.selected = true;
      data.keyOrigin = 'B';
      this.playerRequest.put(data.id, data).subscribe();
    }
    this.createTreeBrackets();
  }

  createTreeBrackets(): void {
    this.playersKeyA = this.techs.filter(item => {
      return item.keyOrigin === 'A';
    });

    this.playersKeyB = this.techs.filter(item => {
      return item.keyOrigin === 'B';
    });
  }

}
