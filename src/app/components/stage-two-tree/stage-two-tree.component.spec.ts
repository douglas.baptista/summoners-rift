import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StageTwoTreeComponent } from './stage-two-tree.component';

describe('StageTwoTreeComponent', () => {
  let component: StageTwoTreeComponent;
  let fixture: ComponentFixture<StageTwoTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StageTwoTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StageTwoTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
