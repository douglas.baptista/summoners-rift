import { TestBed } from '@angular/core/testing';

import { Players.ResourcesService } from './players-resources.service';

describe('Players.ResourcesService', () => {
  let service: Players.ResourcesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Players.ResourcesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
