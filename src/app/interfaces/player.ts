export interface Player {
  id?: string;
  name?: string;
  status?: string;
  keyOrigin?: string;
  img?: string;
  selected?: boolean;
}
